package com.shitbot.command.impl;

import com.shitbot.command.Command;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class PurgeCommand extends Command {
    public PurgeCommand() {
        super("purge");
    }

    @Override
    public void onCommand(GuildMessageReceivedEvent event, List<String> arguments) {
        try {
            for (Message message : event.getChannel().getHistoryFromBeginning(100).submit().get(5, TimeUnit.SECONDS).getRetrievedHistory()) {
                message.delete().queue();
                System.out.println("Deleting: " + message.getContentDisplay());
            }
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
    }
}
