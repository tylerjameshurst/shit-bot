package com.shitbot.command;

import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public abstract class Command {
    private final String identifer;

    //TODO Write a more details command handler
    protected Command(String identifer) {
        this.identifer = identifer;
    }

    public abstract void onCommand(GuildMessageReceivedEvent event, List<String> arguments);

    public String getIdentifer() {
        return identifer;
    }
}
