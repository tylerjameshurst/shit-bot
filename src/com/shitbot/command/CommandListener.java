package com.shitbot.command;

import com.shitbot.command.impl.*;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CommandListener extends ListenerAdapter {
    private static final List<Command> COMMANDS = new LinkedList<>() {{
        add(new GainzCommand());
        add(new PurgeCommand());
        add(new HiscoresCommand());
        add(new StatsCommand());
        add(new HelpCommand());
    }};

    @Override
    public void onGuildMessageReceived(@NotNull GuildMessageReceivedEvent event) {
        if (!event.getMessage().getContentDisplay().startsWith("`"))
            return;
        String command = event.getMessage().getContentDisplay().split("`")[1].split(" ")[0];

        for (Command cmd : COMMANDS) {
            if (cmd.getIdentifer().equalsIgnoreCase(command)) {
                ArrayList<String> arguments = new ArrayList<>(Arrays.asList(event.getMessage().getContentDisplay().split("`")[1].split(" ")));
                arguments.remove(0);
                cmd.onCommand(event, arguments);
                return;
            }
        }

    }
}
