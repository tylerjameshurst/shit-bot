package com.shitbot.profile;

import com.shitbot.Bot;
import com.shitbot.gainz.ImageBuilder;
import com.shitbot.util.StringUtil;
import com.shitbot.util.TimeUtil;
import com.shitbot.util.WebUtil;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Profile {
    private static final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
    private final String name;
    private final TreeMap<Date, String> records = new TreeMap<>();

    public Profile(String name) {
        this.name = name;
        record();
    }

    public static CompletableFuture<Message> checkDaily(Profile profile, TextChannel channel) {
        try {
            ImageBuilder builder = new ImageBuilder(profile);
            BufferedImage image = builder.build();
            File temporary = new File("./resources/" + profile.getName() + ".png");
            ImageIO.write(image, "png", temporary);
            service.schedule(temporary::delete, 2, TimeUnit.SECONDS);
            return channel.sendMessage("``" + StringUtil.formatDisplayName(profile.getName()) + " gained " + NumberFormat.getInstance().format(builder.getDaily()) + " xp today.``").addFile(temporary).submit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void record() {
        Date date = new Date();
        String output = WebUtil.download("https://secure.runescape.com/m=hiscore/index_lite.ws?player=" + name);
        if (output == null || output.isEmpty()) {
            System.err.println("Output for " + name + " was empty or null, cannot save record.");
            return;
        }
        for (Map.Entry<Date, String> entry : records.entrySet()) {
            if (TimeUtil.isSameDay(entry.getKey(), date))
                records.remove(entry.getKey());
        }
        records.put(date, output);
        Bot.PROFILE_LOADER.save(this);
    }

    public Optional<Map.Entry<Date, String>> getLastEntryNotFromToday() {
        return records.entrySet().stream().filter(i -> !TimeUtil.isSameDay(i.getKey(), new Date())).reduce((first, second) -> second);
    }

    public Optional<Map.Entry<Date, String>> getEntryFromDate(Date date) {
        return records.entrySet().stream().filter(e -> TimeUtil.isSameDay(date, e.getKey())).findFirst();
    }

    public Optional<Map.Entry<Date, String>> getFirstEntryFromToday() {
        return records.entrySet().stream().filter(i -> TimeUtil.isSameDay(i.getKey(), new Date())).findFirst();
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Profile))
            return false;
        Profile profile = (Profile) o;
        return name.equals(profile.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
