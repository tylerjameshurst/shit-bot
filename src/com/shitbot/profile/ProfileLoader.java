package com.shitbot.profile;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.shitbot.util.FileUtil;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ProfileLoader {
    private final Gson GSON;

    public ProfileLoader() {
        GsonBuilder builder = new GsonBuilder();
        builder.enableComplexMapKeySerialization();
        builder.serializeNulls();
        builder.setDateFormat(DateFormat.LONG);
        builder.setFieldNamingPolicy(FieldNamingPolicy.IDENTITY);
        builder.setPrettyPrinting();
        builder.setVersion(1.0);
        GSON = builder.create();
    }

    public Profile load(String name) {
        if (!exists(name))
            return new Profile(name);
        File file = new File("./resources/profiles/" + name + ".json");

        Profile profile = deserialize(file.toPath());
        if (profile == null)
            return new Profile(name);

        return profile;
    }

    public void save(Profile profile) {
        Path path = Objects.requireNonNull(FileUtil.getPathOrCreate("./resources/profiles/" + profile.getName() + ".json"));
        try (Writer writer = Files.newBufferedWriter(path)) {
            writer.write(GSON.toJson(profile, Profile.class));
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    public boolean exists(String name) {
        return new File("./resources/profiles/" + name + ".json").exists();
    }

    private Profile deserialize(Path path) {
        try (Reader reader = Files.newBufferedReader(path)) {
            return GSON.fromJson(reader, Profile.class);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Profile> loadAll() {
        List<Profile> list = new ArrayList<>();
        for (File file : Objects.requireNonNull(new File("./resources/profiles/").listFiles()))
            list.add(load(file.getName().replace(".json", "")));
        return list;
    }

}
