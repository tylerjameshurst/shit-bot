package com.shitbot;

import com.shitbot.gainz.service.HiscoresService;
import com.shitbot.gainz.service.ProfileUpdateService;
import com.shitbot.util.TimeUtil;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduler {
    private final ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();

    public void start() {
        service.scheduleAtFixedRate(this::process, 0, 1, TimeUnit.MINUTES);
    }

    private void process() {
        String time = TimeUtil.getGameTime();
        if (time.endsWith(":00"))
            new ProfileUpdateService().run();
        if ("23:55".equals(time)) //End of day - changed to 5 minutes prior just in case there could be some delay
            new HiscoresService(Bot.getDefaultChannel(), "").getMessage();
    }

}
