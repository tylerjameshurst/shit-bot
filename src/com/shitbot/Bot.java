package com.shitbot;

import com.shitbot.command.CommandListener;
import com.shitbot.profile.ProfileLoader;
import com.shitbot.util.Logger;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.utils.Compression;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.util.Objects;

public class Bot {
    public static final ProfileLoader PROFILE_LOADER = new ProfileLoader();
    private static final Scheduler SCHEDULER = new Scheduler();
    public static JDA jda;
    private static boolean TESTING = false;

    public static void main(String[] args) throws LoginException, InterruptedException {
        Logger.configure();
        JDABuilder builder = JDABuilder.createDefault(args[0]);
        builder.disableCache(CacheFlag.MEMBER_OVERRIDES, CacheFlag.VOICE_STATE);
        builder.setCompression(Compression.ZLIB);
        builder.setActivity(Activity.watching("Type `help for a list of commands!"));
        builder.addEventListeners(new CommandListener());
        jda = builder.build().awaitReady();
        SCHEDULER.start();
    }

    public static TextChannel getDefaultChannel() {
        return Objects.requireNonNull(jda.getGuildById(TESTING ? 861281473466466315L : 386377209104826369L)).getDefaultChannel();
    }

}
