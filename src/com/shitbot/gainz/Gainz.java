package com.shitbot.gainz;

import com.shitbot.gainz.stat.StatEntry;
import com.shitbot.gainz.stat.Stats;
import com.shitbot.profile.Profile;
import com.shitbot.util.TimeUtil;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

public class Gainz extends LinkedList<Gain> {
    public Gainz(Profile profile, LocalDate date1, LocalDate date2) {
        List<StatEntry> earlier = StatEntry.build(profile.getEntryFromDate(TimeUtil.convert(date1.isAfter(date2) ? date1 : date2)).orElseThrow());
        List<StatEntry> later = StatEntry.build(profile.getEntryFromDate(TimeUtil.convert(date1.isBefore(date2) ? date1 : date2)).orElseThrow());
        for (int i = 0; i < earlier.size(); i++) {
            StatEntry p1 = later.get(i);
            StatEntry p2 = earlier.get(i);
            Gain gain = new Gain(p1.getStat(), p2.getLevel() - p1.getLevel(), p2.getXp() - p1.getXp(), p2.getRank() - p1.getRank());
            gain.setTotalXP(p2.getXp());
            add(gain);
        }
    }

    public Optional<Gain> getGain(Stats stats) {
        return stream().filter(i -> i.getStat() == stats).findFirst();
    }

}
