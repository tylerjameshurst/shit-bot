package com.shitbot.gainz;

import com.shitbot.gainz.stat.Stats;
import com.shitbot.profile.Profile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDate;

public class ImageBuilder {
    private static BufferedImage source = null;

    static {
        try {
            source = ImageIO.read(new File("./resources/stat_template.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private final Profile profile;
    private BufferedImage image;
    private long daily;

    public ImageBuilder(Profile profile) {
        this.image = source;
        this.profile = profile;
        profile.record();
    }

    public BufferedImage build() {
        for (Gain gain : new Gainz(profile, LocalDate.now(), LocalDate.now().minusDays(1))) {
            image = write(gain.getStat(), gain.getLevels(), gain.getExperience(), gain.getRanks(), gain.getTotalXP());
        }
        return image;
    }

    private BufferedImage write(Stats stat, long levelDiff, long xpDiff, long rankDiff, long totalXP) {
        BufferedImage modified = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D gfx = (Graphics2D) modified.getGraphics();
        gfx.drawImage(image, 0, 0, null);
        gfx.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.4f));
        NumberFormat nf = NumberFormat.getInstance();
        gfx.setColor(levelDiff > 0 ? new Color(0x1FD71C) : Color.WHITE);
        gfx.setFont(new Font(Font.SERIF, Font.BOLD, 12));
        gfx.drawString((levelDiff > 0 ? "+" : "") + nf.format(levelDiff), stat.getX(), stat.getY());

        gfx.setColor(xpDiff > 0 ? new Color(0x1FD71C) : Color.WHITE);
        gfx.setFont(new Font(Font.SERIF, Font.BOLD, 12));
        gfx.drawString((xpDiff > 0 ? "+" : "") + nf.format(xpDiff), stat.getX(), stat.getY() + 15);

        if (stat == Stats.OVERALL) {
            daily = xpDiff;
            gfx.setColor(rankDiff > 0 ? Color.RED : rankDiff < 0 ? new Color(0x1FD71C) : Color.WHITE);
            gfx.setFont(new Font(Font.SERIF, Font.BOLD, 12));
            gfx.drawString((rankDiff > 0 ? "+" : "") + nf.format(rankDiff), stat.getX(), stat.getY() + 30);

            gfx.setColor(Color.WHITE);
            gfx.setFont(new Font(Font.SERIF, Font.BOLD, 12));
            gfx.drawString(new DecimalFormat("####.####").format((double) xpDiff * 100d / (double) totalXP) + "% of total XP!", stat.getX() + 190,
                    stat.getY() + 40);

        }
        return modified;
    }

    public long getDaily() {
        return daily;
    }
}
