package com.shitbot.gainz.service;

import com.shitbot.Bot;
import com.shitbot.gainz.Gainz;
import com.shitbot.gainz.stat.Stats;
import com.shitbot.profile.Profile;
import com.shitbot.util.StringUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.TextChannel;

import java.text.NumberFormat;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.CompletableFuture;

public class HiscoresService {
    private final TextChannel channel;
    private String type;

    public HiscoresService(TextChannel channel, String type) {
        this.channel = channel;
        this.type = type.isEmpty() ? "daily" : type;
    }

    public CompletableFuture<Message> getMessage() {
        EmbedBuilder builder = new EmbedBuilder();
        TreeMap<Long, String> hiscores = new TreeMap<>();
        LocalDate target = LocalDate.now();
        String title = "Today's hiscores: ";
        System.out.println(type);
        switch (type) {
            case "weekly":
                title = "This week's hiscores: ";
                target = target.with(DayOfWeek.MONDAY);
                break;
            case "monthly":
                title = "This month's hiscores: ";
                target = target.withDayOfMonth(1);
                break;
        }
        for (Profile profile : Bot.PROFILE_LOADER.loadAll()) {
            LocalDate now = LocalDate.now();
            Gainz gainz = new Gainz(profile, now, target);
            hiscores.put(gainz.getGain(Stats.OVERALL).orElseThrow().getExperience(), profile.getName());
        }
        builder.setDescription(title);
        builder.setColor(channel.getGuild().getSelfMember().getColor());
        List<Map.Entry<Long, String>> entries = new ArrayList<>(hiscores.entrySet());
        Collections.reverse(entries);
        for (int i = 0; i < entries.size(); i++) {
            Map.Entry<Long, String> entry = entries.get(i);
            builder.addField("#" + (i + 1) + " " + StringUtil.formatDisplayName(entry.getValue()),
                    NumberFormat.getInstance().format(entry.getKey()), false);
        }
        return channel.sendMessage(builder.build()).submit();
    }

}
