package com.shitbot.gainz.stat;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class StatEntry {
    private final Stats stat;
    private final long level, xp, rank;

    public StatEntry(Stats stat, String source) {
        this.stat = stat;
        String[] split = source.split(",");
        this.level = Long.parseLong(split[1]);
        this.xp = Long.parseLong(split[2]);
        this.rank = Long.parseLong(split[0]);
    }

    public static List<StatEntry> build(Map.Entry<Date, String> entry) {
        List<StatEntry> list = new LinkedList<>();
        for (int i = 0; i < 29; i++)
            list.add(new StatEntry(Stats.getStat(i), entry.getValue().split("\n")[i]));
        return list;
    }

    public Stats getStat() {
        return stat;
    }

    public long getLevel() {
        return level;
    }

    public long getXp() {
        return xp;
    }

    public long getRank() {
        return rank;
    }
}
